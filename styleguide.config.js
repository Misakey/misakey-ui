const path = require('path');

module.exports = {
  ignore: ['**/__tests__/**', '**/*.test.{js,jsx,ts,tsx}', '**/*.spec.{js,jsx,ts,tsx}', '**/*.d.ts', '**/components/index.js'],
  styleguideComponents: {
    Wrapper: path.join(__dirname, 'src/utils/DemoWrapper'),
  },
  sections: [
    {
      name: 'Inputs',
      components: 'src/components/Inputs/**/index.js',
      exampleMode: 'collapse',
      usageMode: 'collapse',
    },
    {
      name: 'Other Components',
      components: 'src/components/**/index.js',
      ignore: ['src/components/Inputs/**/index.js'],
      exampleMode: 'collapse', // 'hide' | 'collapse' | 'expand'
      usageMode: 'collapse', // 'hide' | 'collapse' | 'expand'
    },
  ],
};
