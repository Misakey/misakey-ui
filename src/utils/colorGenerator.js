/* eslint no-bitwise: 0 */

function hashCode(str) { // java String#hashCode
  let hash = 0;
  for (let i = 0; i < str.length; i += 1) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash);
  }
  return hash;
}

function intToRGB(i) {
  const c = (i & 0x00FFFFFF).toString(16).toUpperCase();

  return '00000'.substring(0, 6 - c.length) + c;
}

export const stringToRGB = str => intToRGB(hashCode(str));

function hexaToRGB(hexa) {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hexa);
  return result ? {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16),
  } : null;
}

function derivateFromColor(c) {
  let derivated = c / 255.0;
  if (derivated <= 0.03928) {
    derivated /= 12.92;
  } else {
    derivated = ((derivated + 0.055) / 1.055) ** 2.4;
  }
  return derivated;
}

function luminanceFromRGB(r, g, b) {
  return (
    0.2126 * derivateFromColor(r)
    + 0.7152 * derivateFromColor(g)
    + 0.0722 * derivateFromColor(b)
  );
}

function shouldTextBeWhite(hexa) {
  const rgb = hexaToRGB(hexa);
  const luminance = luminanceFromRGB(rgb.r, rgb.g, rgb.g);
  return luminance <= 0.179;
}

export const getBackgroundAndColorFromString = (str) => {
  const hash = hashCode(str);
  const backgroundColor = intToRGB(hash);
  const isTextLight = shouldTextBeWhite(backgroundColor);
  return { backgroundColor, isTextLight };
};
