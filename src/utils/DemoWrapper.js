import React from 'react';
import PropTypes from 'prop-types';

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#e32e72',
    },
    secondary: {
      main: '#C6D8D3',
    },
  },
  typography: {
    useNextVariants: true,
  },
});

const DemoWrapper = (props) => {
  const { children } = props;
  return (
    <MuiThemeProvider theme={theme}>
      {children}
    </MuiThemeProvider>
  );
};

DemoWrapper.propTypes = {
  children: PropTypes.object.isRequired,
};

export default DemoWrapper;
