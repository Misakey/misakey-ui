import * as Yup from 'yup';

export const ERROR_TYPES = {
  required: Symbol('error required'),
  malformed: Symbol('error malformed'),
  notfound: Symbol('error not found'),
  conflict: Symbol('error conflict'),
  unknown: Symbol('error unknown'),
};

export const validation = Yup.string().email(ERROR_TYPES.malformed).required(ERROR_TYPES.required);

export default {
  validation, ERROR_TYPES,
};
