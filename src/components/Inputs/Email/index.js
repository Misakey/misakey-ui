import React from 'react';
import PropTypes from 'prop-types';
import { FormControl, TextField, FormHelperText } from '@material-ui/core';

import { ERROR_TYPES } from './validate';

const TRANSLATION = {
  fr: {
    label: 'Email',
    errors: {
      [ERROR_TYPES.required]: 'Ce champ est obligatoire',
      [ERROR_TYPES.malformed]: 'Merci d\'entrer une adresse email valide',
      [ERROR_TYPES.notfound]: 'Aucun compte lié à cet email',
      [ERROR_TYPES.conflict]: 'Un compte existe déjà avec cet email',
      [ERROR_TYPES.unknown]: 'Une erreur inconnue est survenue',
    },
  },
  en: {
    label: 'Email',
    errors: {
      [ERROR_TYPES.required]: 'Mandatory field',
      [ERROR_TYPES.malformed]: 'The email is invalid',
      [ERROR_TYPES.notfound]: 'No account found for this email',
      [ERROR_TYPES.conflict]: 'An account is already registered with this email',
      [ERROR_TYPES.unknown]: 'Unknown error',
    },
  },
};

/**
 * Email input.
 *
 * **Validation:** required, email format
 */
export const EmailInput = (props) => {
  const {
    value, isTouched, error,
    isSubmitting, language,
    handleChange, handleBlur,
  } = props;

  const isError = Boolean(error && isTouched);

  return (
    <FormControl error={isError} aria-describedby="email-error" fullWidth>
      <TextField
        error={isError}
        disabled={isSubmitting}
        label={TRANSLATION[language].label}
        margin="normal"
        variant="outlined"
        name="email"

        onChange={handleChange}
        onBlur={handleBlur}
        value={value}
        InputProps={{ inputProps: { 'data-matomo-mask': true } }}
      />
      {
        (isError) ? (
          <FormHelperText id="email-error">{TRANSLATION[language].errors[error]}</FormHelperText>
        ) : null
      }
    </FormControl>
  );
};

EmailInput.propTypes = {
  value: PropTypes.string.isRequired,
  isTouched: PropTypes.bool,
  error: PropTypes.symbol,
  isSubmitting: PropTypes.bool,
  handleChange: PropTypes.func.isRequired,
  handleBlur: PropTypes.func,
  language: PropTypes.string,
};

EmailInput.defaultProps = {
  isTouched: false,
  error: undefined,
  isSubmitting: false,
  handleBlur: () => null,
  language: 'fr',
};

export { validation as emailValidation, ERROR_TYPES as EMAIL_ERROR_TYPES } from './validate';


export default EmailInput;
