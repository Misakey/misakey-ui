### Preview
```js
import { validation as emailValidation } from './validate';
import InputDemo from '../_InputDemo';

<InputDemo
  component={EmailInput}
  name="email"
  validation={emailValidation}
  initialValue="john@doe.com"
/>

```
