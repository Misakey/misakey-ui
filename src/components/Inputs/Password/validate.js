import * as Yup from 'yup';

export const ERROR_TYPES = {
  required: Symbol('error required'),
  malformed: Symbol('error malformed'),
  invalid: Symbol('error invalid'),
  unknown: Symbol('error unknown'),
};

export const validation = Yup.string().min(8, ERROR_TYPES.malformed).required(ERROR_TYPES.required);

export default {
  validation, ERROR_TYPES,
};
