### Preview
```js
import { validation as passwordValidation } from './validate';
import InputDemo from '../_InputDemo';

<InputDemo
  component={PasswordInput}
  name="password"
  validation={passwordValidation}
  initialValue="verystrongpassword"
/>

```
