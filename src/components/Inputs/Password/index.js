import React from 'react';
import PropTypes from 'prop-types';
import { FormControl, TextField, FormHelperText } from '@material-ui/core';

import { ERROR_TYPES } from './validate';

const TRANSLATION = {
  fr: {
    label: 'Mot de passe',
    errors: {
      [ERROR_TYPES.required]: 'Ce champs est obligatoire',
      [ERROR_TYPES.malformed]: 'Le mot de passe doit contenir au moins 8 caractères',
      [ERROR_TYPES.invalid]: 'Le mot de passe n\'est pas le bon',
      [ERROR_TYPES.unknown]: 'Une erreur inconnue est survenue',
    },
  },
  en: {
    label: 'Password',
    errors: {
      [ERROR_TYPES.required]: 'Mandatory field',
      [ERROR_TYPES.malformed]: 'At least 8 chars',
      [ERROR_TYPES.invalid]: 'Invalid password',
      [ERROR_TYPES.unknown]: 'Unknown error',
    },
  },
};

/**
 * Password input.
 *
 * **Validation:** required, at least 8 chars
 */
export const PasswordInput = (props) => {
  const {
    value, isTouched, error,
    isSubmitting, language,
    handleChange, handleBlur,
  } = props;

  const isError = Boolean(error && isTouched);

  return (
    <FormControl error={isError} aria-describedby="password-error" fullWidth>
      <TextField
        type="password"
        error={isError}
        disabled={isSubmitting}
        label={TRANSLATION[language].label}
        margin="normal"
        variant="outlined"
        name="password"

        onChange={handleChange}
        onBlur={handleBlur}
        value={value}
        InputProps={{ inputProps: { 'data-matomo-mask': true } }}
      />
      {
        (isError) ? (
          <FormHelperText id="password-error">{TRANSLATION[language].errors[error]}</FormHelperText>
        ) : null
      }
    </FormControl>
  );
};

PasswordInput.propTypes = {
  value: PropTypes.string.isRequired,
  isTouched: PropTypes.bool,
  error: PropTypes.symbol,
  isSubmitting: PropTypes.bool,
  handleChange: PropTypes.func.isRequired,
  handleBlur: PropTypes.func,
  language: PropTypes.string,
};

PasswordInput.defaultProps = {
  isTouched: false,
  error: undefined,
  isSubmitting: false,
  handleBlur: () => null,
  language: 'fr',
};

export { validation as passwordValidation, ERROR_TYPES as PASSWORD_ERROR_TYPES } from './validate';


export default PasswordInput;
