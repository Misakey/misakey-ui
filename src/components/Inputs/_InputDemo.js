import React from 'react';
import PropTypes from 'prop-types';

import * as Yup from 'yup';
// eslint-disable-next-line
import { Formik, Form } from 'formik';

const InputDemo = (props) => {
  const {
    component: InputComponent, name,
    validation, initialValue,
  } = props;

  return (
    <Formik
      initialValues={{ [name]: initialValue }}
      isInitialValid

      onSubmit={(values, actions) => {
        setTimeout(() => {
          actions.setSubmitting(false);
        }, 500);
      }}

      validationSchema={Yup.object().shape({
        [name]: validation,
      })}
    >
      {(formikRenderProps) => {
        const {
          values, touched, errors,
          isSubmitting,
          handleChange, handleBlur,
        } = formikRenderProps;
        return (
          <Form>
            <InputComponent
              value={values[name]}
              isTouched={touched[name]}
              error={errors[name]}
              isSubmitting={isSubmitting}
              handleChange={handleChange}
              handleBlur={handleBlur}
              language="fr"
            />
          </Form>
        );
      }}
    </Formik>
  );
};

InputDemo.propTypes = {
  component: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  validation: PropTypes.object.isRequired,
  initialValue: PropTypes.string.isRequired,
};

export default InputDemo;
