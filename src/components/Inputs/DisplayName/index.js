import React from 'react';
import PropTypes from 'prop-types';
import { FormControl, TextField, FormHelperText } from '@material-ui/core';

import { ERROR_TYPES } from './validate';

const TRANSLATION = {
  fr: {
    label: 'Nom public',
    errors: {
      [ERROR_TYPES.required]: 'Ce champs est obligatoire',
      [ERROR_TYPES.invalid]: 'Le nom public doit comporter entre 3 et 20 caractères, composés de lettres, de chiffres et d\'espaces',
    },
    hint: 'Vous pouvez choisir un nom entre 3 et 20 caractères, composés de lettres, de chiffres et d\'espaces.',
  },
  en: {
    label: 'Display Name',
    errors: {
      [ERROR_TYPES.required]: 'Mandatory field',
      [ERROR_TYPES.invalid]: 'The display name is invalid',
    },
    hint: 'You can choose a name between 3 et 20 chars',
  },
};

/**
 * DisplayName input.
 *
 * **Validation:** 3-20 chars, A-Za-z0-9 and spaces
 */
export const DisplayNameInput = (props) => {
  const {
    value, isTouched, error,
    isSubmitting, language,
    handleChange, handleBlur,
  } = props;

  const isError = Boolean(error && isTouched);

  return (
    <FormControl error={isError} aria-describedby="displayName-error" fullWidth>
      <TextField
        error={isError}
        disabled={isSubmitting}
        label={TRANSLATION[language].label}
        margin="normal"
        variant="outlined"
        name="displayName"

        onChange={handleChange}
        onBlur={handleBlur}
        value={value}
        InputProps={{ inputProps: { 'data-matomo-mask': true } }}
      />
      {
        (isError) ? (
          <FormHelperText id="displayName-error">{TRANSLATION[language].errors[error]}</FormHelperText>
        ) : <FormHelperText id="displayName-error">{TRANSLATION[language].hint}</FormHelperText>
      }
    </FormControl>
  );
};

DisplayNameInput.propTypes = {
  value: PropTypes.string.isRequired,
  isTouched: PropTypes.bool,
  error: PropTypes.symbol,
  isSubmitting: PropTypes.bool,
  handleChange: PropTypes.func.isRequired,
  handleBlur: PropTypes.func,
  language: PropTypes.string,
};

DisplayNameInput.defaultProps = {
  isTouched: false,
  error: undefined,
  isSubmitting: false,
  handleBlur: () => null,
  language: 'fr',
};

export { validation as displayNameValidation, ERROR_TYPES as DISPLAY_NAME_ERROR_TYPES } from './validate';


export default DisplayNameInput;
