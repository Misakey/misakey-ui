### Preview
```js
import { validation as displayNameValidation } from './validate';
import InputDemo from '../_InputDemo';

<InputDemo
  component={DisplayNameInput}
  name="displayName"
  validation={displayNameValidation}
  initialValue="John Doe"
/>
```
