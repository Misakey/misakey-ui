import * as Yup from 'yup';

export const ERROR_TYPES = {
  required: Symbol('error required'),
  invalid: Symbol('error invalid'),
};

const validationRegex = /^([A-Za-z0-9 ])+$/;

export const validation = Yup
  .string()
  .matches(validationRegex, { message: ERROR_TYPES.invalid })
  .min(3, ERROR_TYPES.invalid)
  .max(20, ERROR_TYPES.invalid)
  .required(ERROR_TYPES.required);

export default {
  validation, ERROR_TYPES,
};
