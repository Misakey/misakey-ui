### Preview
```js
import { validation as otpValidation } from './validate';
import InputDemo from '../_InputDemo';

<InputDemo
  component={OtpInput}
  name="otp"
  validation={otpValidation}
  initialValue="123123"
/>
```
