import * as Yup from 'yup';

export const ERROR_TYPES = {
  required: Symbol('error required'),
  malformed: Symbol('error malformed'),
  invalid: Symbol('error invalid'),
};

const validationRegex = /^[0-9]{6}$/;

export const validation = Yup
  .string()
  .matches(validationRegex, { message: ERROR_TYPES.invalid })
  .required(ERROR_TYPES.required);

export default {
  validation, ERROR_TYPES,
};
