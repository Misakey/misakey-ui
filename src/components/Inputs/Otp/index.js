import React from 'react';
import PropTypes from 'prop-types';
import { FormControl, TextField, FormHelperText } from '@material-ui/core';

import { ERROR_TYPES } from './validate';

const TRANSLATION = {
  fr: {
    label: 'Code de confirmation',
    errors: {
      [ERROR_TYPES.required]: 'Ce champs est obligatoire',
      [ERROR_TYPES.malformed]: 'Le code de confirmation doit contenir 6 chiffres',
      [ERROR_TYPES.invalid]: 'Le code de confirmation est incorrect',
      [ERROR_TYPES.expired]: 'Le code de confirmation a expiré. Nous vous en avons envoyé un nouveau',
    },
  },
  en: {
    label: 'Confirmation code',
    errors: {
      [ERROR_TYPES.required]: 'Mandatory field',
      [ERROR_TYPES.malformed]: 'Should be 6 digits',
      [ERROR_TYPES.invalid]: 'Le code de confirmation est incorrect',
      [ERROR_TYPES.expired]: 'Expired code, we send you a new one',
    },
  },
};

/**
 * OTP input.
 *
 * **Validation:** 6 Digits
 */
export const OtpInput = (props) => {
  const {
    value, isTouched, error,
    isSubmitting, language,
    handleChange, handleBlur,
  } = props;

  const isError = Boolean(error && isTouched);

  return (
    <FormControl error={isError} aria-describedby="otp-error" fullWidth>
      <TextField
        error={isError}
        disabled={isSubmitting}
        label={TRANSLATION[language].label}
        margin="normal"
        variant="outlined"
        name="otp"

        onChange={handleChange}
        onBlur={handleBlur}
        value={value}
        InputProps={{ inputProps: { 'data-matomo-mask': true } }}
      />
      {
        (isError) ? (
          <FormHelperText id="otp-error">{TRANSLATION[language].errors[error]}</FormHelperText>
        ) : null
      }
    </FormControl>
  );
};

OtpInput.propTypes = {
  value: PropTypes.string.isRequired,
  isTouched: PropTypes.bool,
  error: PropTypes.symbol,
  isSubmitting: PropTypes.bool,
  handleChange: PropTypes.func.isRequired,
  handleBlur: PropTypes.func,
  language: PropTypes.string,
};

OtpInput.defaultProps = {
  isTouched: false,
  error: undefined,
  isSubmitting: false,
  handleBlur: () => null,
  language: 'fr',
};

export { validation as otpValidation, ERROR_TYPES as OTP_ERROR_TYPES } from './validate';


export default OtpInput;
