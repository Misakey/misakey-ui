import React from 'react';
import PropTypes from 'prop-types';

import { Avatar } from '@material-ui/core';

import { getBackgroundAndColorFromString } from '../../utils/colorGenerator';

/**
 * Colorized Avatar is a circular avatar that will display image if set and
 * first letter of text (name / email) if not set.
 * If displaying text, will generate a color from a hash of the text.
 */
const ColorizedAvatar = (props) => {
  const { text, image, ...rest } = props;

  if (image) {
    return <Avatar src={image} {...rest} />;
  }
  const { backgroundColor, isTextLight } = getBackgroundAndColorFromString(text);
  return (
    <Avatar
      style={{
        backgroundColor: `#${backgroundColor}`,
        color: (isTextLight) ? 'white' : 'black',
      }}
      {...rest}
    >
      {text.charAt(0)}
    </Avatar>
  );
};

ColorizedAvatar.propTypes = {
  /** The image to use as avatar. If not present, will use first letter of text */
  image: PropTypes.string,
  /** The text to derivate the color from, and to extract the first letter. */
  text: PropTypes.string.isRequired,
};

ColorizedAvatar.defaultProps = {
  image: '',
};

export default ColorizedAvatar;
